Reference Materials for Reading Course “Data Science Methods in Investment Decision Making”
1.	Literature on data-driven methods for option hedging and pricing
HANS BUEHLER, LUKAS GONON, JOSEF TEICHMANN, AND BEN WOOD (2018). DEEP HEDGING. Working paper, JP Morgan
 Jay Cao, Jacky Chen, and John Hull (2019). A Neural Network Approach to Understanding Implied Volatility Movements. Working paper, Joseph L. Rotman School of Management University of Toronto 
Chui, Haili (2001), Non-Rigid Point Matching: Algorithms, Extensions and Applications, Yale University, New Haven, CT, USA, CiteSeerX 10.1.1.109.6855
J. Duchon (1976), Splines minimizing rotation invariant semi-norms in Sobolev spaces. pp 85–100, In: Constructive Theory of Functions of Several Variables, Oberwolfach 1976, W. Schempp and K. Zeller, eds., Lecture Notes in Math., Vol. 571, Springer, Berlin, 1977. doi:10.1007/BFb0086566
Hagan, P.S., Kumar, D., Lesniewski, A.S., Woodward, D.E. (2002). Managing smile risk. Wilmott Magazine, pp84-108.
Heston, S.L. (1993). A closed form solution for options with stochastic volatility with applications to bonds and currency options, Review of Financial Studies, 6(2}:327-343.
Hull, J. C. and White A. (1987). The pricing of options on assets with stochastic volatilities,  Journal of Finance, 42:281-300.
Hull, J and White A. (2017). Optimal delta hedging for options, J Bank Financ, 82: 180-190.
Hutchinson, J. M., Lo, A. W. and Poggio, T. (1994). A nonparametric approach to pricing and hedging derivative securities via learning networks. The Journal of Finance, 49(3):851–889.
Shuaiqiang Liu, Anastasia Borovykh, Lech A. Grzelak, and Cornelis W. Oosterlee1 (2019). A neural network-based framework for financialmodel calibration. Working paper, Delft University of Technology, Delft, the Netherlands
Shuaiqiang Liu 1,*, CornelisW. Oosterlee 1,2 and Sander M.Bohte 2 (2018). Pricing options and computing implied volatilities using neural networks. Working paper, Delft University of Technology, Netherlands.
Malliaris, M. and Salchenberger L. (1993). Beating the best: a neural network challenges the Black-Scholes formula. In Proceedings of 9th IEEE Conference on Artificial Intelligence for Applications, pp 445–449. IEEE.
Nian, K. Coleman, T. F. and Li Y. (2018). Learning minimum variance discrete hedging directly from the market, Quantitative Finance, 18(7): 1115-1128.
Ruf, J. and Wang, W. (2020). Neural networks for option pricing and hedging: a literature review. Journal of Computational Finance, Forthcoming.
Ackerer, D., N. Tagasovska and T. Vatter (2020). Deep Smoothing of the Implied Volatility Surface. Working paper, UBS, Switzerland.
Zheng, K., Luo, X. and Zhang, J. (2012) The Relation between SPX at-the-money Implied Volatility and VIX. Working paper, HSBC School of Business, Peking University.
2.	Literature on factor model for asset pricing
Fama, E. F.; French, K. R. (1993). "Common risk factors in the returns on stocks and bonds". Journal of Financial Economics. 33: 3–56. CiteSeerX 10.1.1.139.5892. doi:10.1016/0304-405X(93)90023-5.
Fama, E., and K. French. “A Five-Factor Asset Pricing Model.” Journal of Financial Economics, 116 (2015), pp. 1-22. Working Paper (September 2014) available at SSRN (free download)
Daniel, Kent D. and Hirshleifer, David A. and Sun, Lin, Short- and Long-Horizon Behavioral Factors (April 8, 2019). Review of Financial Studies, Forthcoming, Available at SSRN: https://ssrn.com/abstract=3086063 or http://dx.doi.org/10.2139/ssrn.3086063
Carhart, M. M. (1997). "On Persistence in Mutual Fund Performance". The Journal of Finance. 52 (1): 57–82. doi:10.1111/j.1540-6261.1997.tb03808.x. JSTOR 2329556.
Hou, Kewei and Xue, Chen and Zhang, Lu, Digesting Anomalies: An Investment Approach (October 1, 2014). Review of Financial Studies, Forthcoming, Fisher College of Business Working Paper No. WP 2012-03-021, Dice Center Working Paper No. 2012-21, Available at SSRN: https://ssrn.com/abstract=2508322
Stambaugh, Robert F. and Yu, Jianfeng and Yuan, Yu, Arbitrage Asymmetry and the Idiosyncratic Volatility Puzzle (October 12, 2014). Journal of Finance, Vol 70, pp 1903-1948, 2015, Jacobs Levy Equity Management Center for Quantitative Financial Research Paper , Available at SSRN: https://ssrn.com/abstract=2155491 or http://dx.doi.org/10.2139/ssrn.2155491
Chapter 5, Campbell JY, Lo AW, MacKinlay CA, Adamek P, Viceira LM. The Econometrics of Financial Markets. Princeton, NJ: Princeton University Press; 1997.
William H. Greene, Econometric Analysis , 8th Edition 
Narasimhan Jegadeesh and Sheridan Titman, Returns to Buying Winners and Selling Losers: Implications for Stock Market Efficiency.The Journal of Finance Vol. 48, No. 1 (Mar., 1993), pp. 65-91 (27 pages)
Jegadeesh, Narasimhan and Titman, Sheridan, Profitability of Momentum Strategies: an Evaluation of Alternative Explanations (June 1999). NBER Working Paper No. w7159, Available at SSRN: https://ssrn.com/abstract=227349
Paresh Kumar Narayan  Huson Ali Ahmed  Seema Narayan.  Do Momentum‐Based Trading Strategies Work in the Commodity Futures Markets? May 2014 Journal of Futures Markets 35(9)
Geczy, Christopher Charles and Samonov, Mikhail, Two Centuries of Price Return Momentum (January 25, 2016). Financial Analysts Journal, Vol. 72, No. 5 (September/October 2016), Jacobs Levy Equity Management Center for Quantitative Financial Research Paper , Available at SSRN: https://ssrn.com/abstract=2292544 or http://dx.doi.org/10.2139/ssrn.2292544
Daniel, Kent, and Tobias J. Moskowitz. "Momentum crashes." Journal of Financial Economics 122.2 (2016): 221-247.
#Liu, Laura Xiaolei and Zhang, Lu, Momentum Profits, Factor Pricing, and Macroeconomic Risk. Review of Financial Studies, Forthcoming, Available at SSRN: https://ssrn.com/abstract=1027214


