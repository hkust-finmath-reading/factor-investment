In this chapter, we use paper to explain intuition of the following factor
Market factor(CAPM)
Scale factor
value factor
momentum factor
1. existence of this factor
#Narasimhan Jegadeesh and Sheridan Titman, Returns to Buying Winners and Selling Losers: Implications for Stock Market Efficiency.The Journal of Finance Vol. 48, No. 1 (Mar., 1993), pp. 65-91 (27 pages)
#Jegadeesh, Narasimhan and Titman, Sheridan, Profitability of Momentum Strategies: an Evaluation of Alternative Explanations (June 1999). NBER Working Paper No. w7159, Available at SSRN: https://ssrn.com/abstract=227349
#Paresh Kumar Narayan  Huson Ali Ahmed  Seema Narayan.  Do Momentum‐Based Trading Strategies Work in the Commodity Futures Markets? May 2014 Journal of Futures Markets 35(9)
2. explaination of this factor
#Daniel, Kent, and Tobias J. Moskowitz. "Momentum crashes." Journal of Financial Economics 122.2 (2016): 221-247.
#Kent Daniel, David Hirshleifer, & Avanidhar Subrahmanyam. (1998). Investor Psychology and Security Market under- and Overreactions. The Journal of Finance (New York), 53(6), 1839-1885.
#Geczy, Christopher Charles and Samonov, Mikhail, Two Centuries of Price Return Momentum (January 25, 2016). Financial Analysts Journal, Vol. 72, No. 5 (September/October 2016), Jacobs Levy Equity Management Center for Quantitative Financial Research Paper , Available at SSRN: https://ssrn.com/abstract=2292544 or http://dx.doi.org/10.2139/ssrn.2292544

#Liu, Laura Xiaolei and Zhang, Lu, Momentum Profits, Factor Pricing, and Macroeconomic Risk. Review of Financial Studies, Forthcoming, Available at SSRN: https://ssrn.com/abstract=1027214
dividend factor
turnover rate factor
open interest rate factor
etc

we try to find out the logic behind those factor, and build a mathematical model(market microstructure level) for each of them to explain why they are correlated with asset price.
We can find a paper for each of those factors. 
